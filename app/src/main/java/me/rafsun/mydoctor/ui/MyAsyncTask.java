package me.rafsun.mydoctor.ui;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.glassfish.tyrus.client.ClientManager;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import me.rafsun.mydoctor.R;
import me.rafsun.mydoctor.Utils.CMD;
import me.rafsun.mydoctor.Utils.URLs;
import me.rafsun.mydoctor.Utils.WSUtil;
import me.rafsun.mydoctor.json.Appointment;
import me.rafsun.mydoctor.json.AppointmentResponse;
import me.rafsun.mydoctor.json.DoctorSaveEventResponse;
import me.rafsun.mydoctor.json.DoctorSearchAllPatientsResponse;
import me.rafsun.mydoctor.json.InfoResponse;

/**
 * Created by Rafsun on 2/25/15.
 */
public abstract class MyAsyncTask extends AsyncTask<String, Void, List<Object>> {


    private static CountDownLatch messageLatch;
    private List<Object> listOfResponseObjects;
    protected Context context;
    private int numOfCountDownLatch;

    public MyAsyncTask(Context context, int numOfCountDownLatch) {
        this.context = context;
        this.numOfCountDownLatch = numOfCountDownLatch;
    }

    @Override
    protected List<Object> doInBackground(final String... urls) {


        try {
            listOfResponseObjects = new ArrayList<>(urls.length);
            messageLatch = new CountDownLatch(numOfCountDownLatch);

            final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();
            final Gson gson = new Gson();
            ClientManager client = ClientManager.createClient();
            Log.i("TEST", "### 1 AsyncTask.doInBackground");

            client.connectToServer(new Endpoint() {

                @Override
                public void onOpen(final Session session, EndpointConfig config) {
                    try {

                        session.addMessageHandler(new MessageHandler.Whole<String>() {

                            @Override
                            public void onMessage(String message) {
                                Log.d("TEST", message);
                                JsonElement jdata = new JsonParser().parse(message);


                                if (WSUtil.isReceived(jdata)) {
                                    messageLatch.countDown();

                                } else if (WSUtil.isToken(jdata)) {
                                    if (WSUtil.isTokenPositive(jdata)) {

                                        if (WSUtil.isLoginResponse(jdata)) {

                                            if (WSUtil.isLoginSuccessful(jdata)) {
                                                try {

                                                    for (int i = 0; i < urls.length; i++) {

                                                        session.getBasicRemote().sendText(urls[i]);

                                                    }
//                                        session.getBasicRemote().sendText(SENT_GET_ALL_APPOINTMENTS);
//                                        session.getBasicRemote().sendText(SENT_MESSAGE_DOCTOR_INFO);
                                                } catch (IOException e) {
                                                    Log.i("sendText", e.toString());
                                                    System.out.println("BACKGROUND " + e);
                                                    showToast(context.getString(R.string.async_failed));
                                                }
                                            } else {
                                                showToast(context.getString(R.string.aync_wrong_user_pass));
                                                while (messageLatch.getCount() > 0)
                                                    messageLatch.countDown();

                                            }

                                        } else if (WSUtil.isSearchAllPatientsResponse(jdata)) {
                                            DoctorSearchAllPatientsResponse doctorSearchAllPatientsResponse = gson.fromJson(jdata, DoctorSearchAllPatientsResponse.class);
                                            listOfResponseObjects.add(doctorSearchAllPatientsResponse);
//                                    for (Patient patient : doctorSearchAllPatientsResponse.getSearchAllPatientsResponse().getPatients()) {
//                                        System.out.println(patient);
//                                        System.out.println("\n\n\n");
//                                    }
//
//                                    System.out.println(doctorSearchAllPatientsResponse.getSearchAllPatientsResponse().getPatients().size());

                                        } else if (WSUtil.isDoctorInfoResponse(jdata)) {
                                            InfoResponse infoResponse = gson.fromJson(jdata, InfoResponse.class);

                                            //return as anonymous object , later cast it.
                                            listOfResponseObjects.add(infoResponse);
//                                    System.out.println(infoResponse.getDoctorInfoResponse().getAddress());
                                        } else if (WSUtil.isDoctorAppointmentsResponse(jdata)) {

                                            AppointmentResponse appointmentResponse
                                                    = gson.fromJson(jdata, AppointmentResponse.class);
                                            for (Appointment apt : appointmentResponse.getDoctorAppointmentsResponse().getAppointments()) {
//                                        System.out.println(apt.getStart() + "-------" + apt.getEnd());
                                                Date ds = WSUtil.parseDate(apt.getStart());
                                                apt.setStartDate(ds);
                                                Date de = WSUtil.parseDate(apt.getEnd());
                                                apt.setEndDate(de);

                                            }
                                            //return as anonymous object , later cast it.
                                            listOfResponseObjects.add(appointmentResponse);

                                        } else if (WSUtil.isSaveEventResponse(jdata)) {

                                            DoctorSaveEventResponse doctorSaveEventResponse
                                                    = gson.fromJson(jdata, DoctorSaveEventResponse.class);

                                            for (Appointment apt : doctorSaveEventResponse.getSaveEventResponse().getEvents()) {
//                                        System.out.println(apt.getStart() + "-------" + apt.getEnd());
                                                Date ds = WSUtil.parseDate(apt.getStart());
                                                apt.setStartDate(ds);
                                                Date de = WSUtil.parseDate(apt.getEnd());
                                                apt.setEndDate(de);

                                            }

                                            //return as anonymous object , later cast it.
                                            listOfResponseObjects.add(doctorSaveEventResponse);
                                        } else if (WSUtil.isSearchAllPatientsResponse(jdata)) {
//                                    System.out.println(message);
                                        }

                                        if (messageLatch.getCount() > 0)
                                        messageLatch.countDown();

                                    }
                                }


                            }

                        });
                        session.getBasicRemote().sendText(URLs.getCommandJson(CMD.LOGIN, null));

                    } catch (IOException e) {

                        System.out.println("BACKGROUND " + e);
                        showToast(context.getString(R.string.async_debug_onopen));

                    }
                }
            }, cec, URI.create(URLs.getRootURL()));

            messageLatch.await(URLs.TIME_OUT, TimeUnit.SECONDS);

            return listOfResponseObjects;
        } catch (Exception ex) {

            System.out.println("BACKGROUND " + ex);
            showToast(context.getString(R.string.aync_debug_failed_con_server));


        }
        return null;
    }


    private void showToast(final String message) {
        final MainActivity activity = (MainActivity) context;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onPostExecute(List<Object> listOfResponseAsObjects) {
        super.onPostExecute(listOfResponseAsObjects);
        updateUI(listOfResponseAsObjects);
    }


    abstract protected void updateUI(List<Object> responseAsObjects);


}
