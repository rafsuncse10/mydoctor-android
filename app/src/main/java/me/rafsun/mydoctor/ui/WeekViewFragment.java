package me.rafsun.mydoctor.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dd.CircularProgressButton;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;
import me.rafsun.mydoctor.MyWeekView.WeekView;
import me.rafsun.mydoctor.MyWeekView.WeekViewEvent;
import me.rafsun.mydoctor.R;
import me.rafsun.mydoctor.Singletones.PatientInfoSingletone;
import me.rafsun.mydoctor.Singletones.WeekEventSingletone;
import me.rafsun.mydoctor.Utils.CMD;
import me.rafsun.mydoctor.Utils.URLs;
import me.rafsun.mydoctor.Utils.Utils;
import me.rafsun.mydoctor.Utils.WSUtil;
import me.rafsun.mydoctor.json.Appointment;
import me.rafsun.mydoctor.json.AppointmentResponse;
import me.rafsun.mydoctor.json.DoctorAppointmentsResponse;
import me.rafsun.mydoctor.json.DoctorInfoResponse;
import me.rafsun.mydoctor.json.DoctorSaveEventResponse;
import me.rafsun.mydoctor.json.DoctorSearchAllPatientsResponse;
import me.rafsun.mydoctor.json.InfoResponse;
import me.rafsun.mydoctor.json.Patient;
import me.rafsun.mydoctor.json.PatientName;
import me.rafsun.mydoctor.json.SaveEventResponse;

/**
 * Created by Rafsun on 3/3/15.
 */
public class WeekViewFragment extends Fragment implements WeekView.MonthChangeListener,
        WeekView.EventClickListener, WeekView.EventDoubleClickListener, WeekView.EventLongPressListener, WeekView.EmptyViewClickListener, SwipeRefreshLayout.OnRefreshListener {


/**
 * A placeholder fragment containing a simple view.
 */

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_THREE_DAY_VIEW = 2;
    private static final int TYPE_WEEK_VIEW = 3;
    private int mWeekViewType = TYPE_THREE_DAY_VIEW;
    private WeekView mWeekView;
    private SwipeRefreshLayout swiper;
    private FrameLayout frameLayout;
    // Populate the week view with some events.
    private static List<WeekViewEvent> events;
    String event_date_str;
    String event_startTime_str;
    String event_endTime_str;
    String event_patient_str;
    String event_phone_str;
    String event_notes_str;
    String event_email_str;
    private List<Patient> patients;
    private Context context;

    private int selectedStatusItem;
    private MaterialDialog mMaterialDialog;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static WeekViewFragment newInstance(int sectionNumber) {
        WeekViewFragment fragment = new WeekViewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public WeekViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);


        events = WeekEventSingletone.INSTANCE.getInstance();
        patients = PatientInfoSingletone.INSTANCE.getInstance();
//        System.out.println("DATES : " + events.isEmpty());
        //////////////////////////////
        //  Setup Swipe To Refresh  //
        //////////////////////////////
        swiper = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swiper.setSize(SwipeRefreshLayout.LARGE);
        swiper.setColorSchemeResources(
                R.color.holo_blue_bright,
                R.color.holo_orange_light,
                R.color.holo_green_light,
                R.color.holo_red_light
        );

        swiper.setOnRefreshListener(this);

        frameLayout = (FrameLayout) rootView.findViewById(R.id.weekViewFrameLayout);
        //add WeekView Programmatically
        addWeekView();

        Utils.hideKeyboardOnOutSideTouch(rootView, getActivity());


        checkToReloadForSavedCredential();
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

        context = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    int getPixels(int TYPE_VALUE_UNIT, float size) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TYPE_VALUE_UNIT, size, metrics);
    }


    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {

        //events not populated with data
        if (events.isEmpty()) return events;

        List<WeekViewEvent> events1 = getEvents(newYear, newMonth);
        return events1;
    }

    private List<WeekViewEvent> getDummyEvents(int newYear, int newMonth) {

        List<WeekViewEvent> events = new ArrayList<>();
        events.add(getDummyEvent(newYear, newMonth));
        return events;
    }

    private WeekViewEvent getDummyEvent(int newYear, int newMonth) {
        Calendar startTime;

        Calendar endTime;

        startTime = Calendar.getInstance();
        startTime.set(Calendar.DAY_OF_MONTH, 1);
        startTime.set(Calendar.HOUR_OF_DAY, 3);
        startTime.set(Calendar.MONTH, newMonth - 1);
        startTime.set(Calendar.YEAR, newYear);
        endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.HOUR_OF_DAY, 3);
        WeekViewEvent event = new WeekViewEvent(1, getEventTitle(null, startTime, endTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_03));
        return event;
    }

    private List<WeekViewEvent> getEvents(int newYear, int newMonth) {

        List<WeekViewEvent> eventsTemp = new ArrayList<>();

        for (WeekViewEvent event : events) {
            if ((event.getStartTime().get(Calendar.MONTH) == (newMonth - 1)) && (event.getStartTime().get(Calendar.YEAR) == newYear))

                eventsTemp.add(event);
        }

        if (eventsTemp.isEmpty())
            eventsTemp.add(getDummyEvent(newYear, newMonth));

        return eventsTemp;
    }


    private void addWeekView() {
        mWeekView = new WeekView(getActivity(), null, 0, swiper);
        mWeekView.setEventTextColor(Color.WHITE);
        mWeekView.setTextSize(getPixels(TypedValue.COMPLEX_UNIT_SP, 12));
        mWeekView.setHourHeight(getPixels(TypedValue.COMPLEX_UNIT_DIP, 60));
        mWeekView.setHeaderColumnPadding(getPixels(TypedValue.COMPLEX_UNIT_DIP, 8));
        mWeekView.setHeaderColumnTextColor(Color.parseColor("#8f000000"));
        mWeekView.setHeaderRowPadding(getPixels(TypedValue.COMPLEX_UNIT_DIP, 12));
        mWeekView.setColumnGap(getPixels(TypedValue.COMPLEX_UNIT_DIP, 8));
        mWeekView.setNumberOfVisibleDays(3);
        mWeekView.setHeaderRowBackgroundColor(Color.parseColor("#ffefefef"));
        mWeekView.setDayBackgroundColor(Color.parseColor("#05000000"));
        mWeekView.setTodayBackgroundColor(Color.parseColor("#1848adff"));
        mWeekView.setHeaderColumnBackgroundColor(Color.parseColor("#ffffffff"));
        mWeekView.goToHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        frameLayout.addView(mWeekView, layoutParams);


        // Show a toast message about the touched event.
        mWeekView.setOnEventClickListener(this);
        mWeekView.setOnEventDoubleClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);

        mWeekView.setEmptyViewClickListener(this);
    }

    //
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.main, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_today:
                mWeekView.goToToday();
                return true;
            case R.id.action_day_view:
                if (mWeekViewType != TYPE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(1);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_three_day_view:
                if (mWeekViewType != TYPE_THREE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_THREE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(3);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_week_view:
                if (mWeekViewType != TYPE_WEEK_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_WEEK_VIEW;
                    mWeekView.setNumberOfVisibleDays(7);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void checkToReloadForSavedCredential() {
        if (!patients.isEmpty())
            return;

        if (Utils.isSavedAtSharedPref(getActivity())) {
            SharedPreferences editor = getActivity().getSharedPreferences(Utils.PREF_KEY_LOG, Context.MODE_PRIVATE);
            String Username = editor.getString(Utils.PREF_USER, "");
            String SHAPass = editor.getString(Utils.PREF_PASS, "");
            if (Username.isEmpty() || SHAPass.isEmpty()) {
                Toast.makeText(getActivity(), getActivity().getString(R.string.weekviewfrag_shared_pref_err), Toast.LENGTH_SHORT).show();
            } else {
                URLs.setUsername(Username);
                URLs.setSHAPassword(SHAPass);
                swiper.post(new Runnable() {
                    @Override
                    public void run() {
                        swiper.setRefreshing(true);
                    }
                });
                onRefresh();

            }
        }
    }

    @Override
    public void onRefresh() {


        final int NUMBER_OF_TASK = 2;

        final MyAsyncTask asyncTask = new MyAsyncTask(getActivity(), ((NUMBER_OF_TASK * 2) + 2)) {
            @Override
            protected void updateUI(List<Object> responseAsObjects) {


                if (responseAsObjects != null && !responseAsObjects.isEmpty()) {

                    DoctorInfoResponse doctorInfoResponse;
                    DoctorAppointmentsResponse doctorAppointmentsResponse;
                    DoctorSearchAllPatientsResponse doctorSearchAllPatientsResponse;
                    for (int i = 0; i < responseAsObjects.size(); i++) {   //NUMBER_OF_TASK
                        Object responseAsObject = responseAsObjects.get(i);
//                    if (responseAsObject != null) {

                        if (responseAsObject instanceof DoctorSearchAllPatientsResponse) {
                            doctorSearchAllPatientsResponse = ((DoctorSearchAllPatientsResponse) responseAsObject);

                            if (!patients.isEmpty()) patients.clear();
                            patients.addAll(doctorSearchAllPatientsResponse.getSearchAllPatientsResponse().getPatients());


//                        tv.setText(responseAsObject != null ? doctorInfoResponse.toString() : "@null");
                        } else if (responseAsObject instanceof InfoResponse) {

                            doctorInfoResponse = ((InfoResponse) responseAsObject).getDoctorInfoResponse();
//                        tv.setText(responseAsObject != null ? doctorInfoResponse.toString() : "@null");
                        } else if (responseAsObject instanceof AppointmentResponse) {
                            doctorAppointmentsResponse = ((AppointmentResponse) responseAsObject).getDoctorAppointmentsResponse();
//                        tv.setText(responseAsObject != null ? doctorAppointmentsResponse.toString() : "@null");

                            addEvents(doctorAppointmentsResponse.getAppointments());
                        } else {
                            Toast.makeText(this.context, context.getString(R.string.failed_retry), Toast.LENGTH_LONG).show();
                        }
                    }

                } else {
                    Toast.makeText(this.context, context.getString(R.string.failed_retry), Toast.LENGTH_LONG).show();
                }
                //show swiper at startup - http://stackoverflow.com/a/26910973

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        swiper.setRefreshing(false);
                    }
                });


            }
        };

//        asyncTask.execute(URLs.getCommandJson(CMD.PATIENTS_APPOINTMENT_GET, null), URLs.getCommandJson(CMD.DOCTOR_INFO_GET, null));
        asyncTask.execute(URLs.getCommandJson(CMD.PATIENTS_APPOINTMENT_GET, null), URLs.getCommandJson(CMD.PATIENTS_INFO_GET, null));


    }

    private void addEvents(List<Appointment> appointments) {


        if (!events.isEmpty()) events.clear();

        WeekViewEvent event;
        Calendar startTime;

        Calendar endTime;


        for (int i = 0; i < appointments.size(); i++) {
            Appointment appointment = appointments.get(i);

            //only for appointment type
            if (appointment.getType().equals("A") && !appointment.getStatus().equals("Cancelado")) {
                startTime = Calendar.getInstance();
                endTime = (Calendar) startTime.clone();
                Date dateStartDate = appointment.getStartDate();
                Date dateEndDate = appointment.getEndDate();
                startTime.setTime(dateStartDate);
                endTime.setTime(dateEndDate);


                event = new WeekViewEvent(i + 1, getEventTitle(appointment, startTime, endTime), startTime, endTime);
                event.setColor(Color.parseColor(WSUtil.getColorAsString(appointment.getStatus())));
                event.setAppointment(appointment);
                events.add(event);
            }
        }


        mWeekView.notifyDatasetChanged();
    }


    public static String getEventTitle(Appointment appointment, Calendar startTime, Calendar endTime) {

        if (appointment == null)
            return String.format(startTime.get(Calendar.HOUR_OF_DAY) + ":" + startTime.get(Calendar.MINUTE)
                            + "-" + endTime.get(Calendar.HOUR_OF_DAY) + ":" + endTime.get(Calendar.MINUTE)
            );

        else
            return String.format(startTime.get(Calendar.HOUR_OF_DAY) + ":" + startTime.get(Calendar.MINUTE)
                    + "-" + endTime.get(Calendar.HOUR_OF_DAY) + ":" + endTime.get(Calendar.MINUTE)
                    + "\n" + appointment.getTitle());


//        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));


    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {

        Appointment appointment = event.getAppointment();
        Patient patientTemp = null;
        for (Patient patient : patients) {
            String nameAppend = new StringBuilder().append("")
                    .append(patient.getName() != null && !patient.getName().isEmpty() ? patient.getName() : "")
                    .append(patient.getName() != null && !patient.getName().isEmpty() && patient.getLastName() != null && !patient.getLastName().isEmpty() ? " " : "")
                    .append(patient.getLastName() != null && !patient.getLastName().isEmpty() ? patient.getLastName() : "")
                    .append(patient.getLastName() != null && !patient.getLastName().isEmpty() && patient.getSurName() != null && !patient.getSurName().isEmpty() ? " " : "")
                    .append(patient.getSurName() != null && !patient.getSurName().isEmpty() ? patient.getSurName() : "")

                    .toString();
            if (nameAppend.equals(appointment.getTitle())) {
                patientTemp = patient;
                break;
            }
        }

        event_date_str = appointment.getStart().split(" ")[0];
        event_startTime_str = appointment.getStart().split(" ")[1];
        event_endTime_str = appointment.getEnd().split(" ")[1];
//        String info = String.format(
//
//                "%s%s%s"
//
//                , (appointment.getTitle() == null || appointment.getTitle().isEmpty()) ? "" : "Name:" + appointment.getTitle() + "\n"
//                , (appointment.getPhone() == null || appointment.getPhone().isEmpty()) ? "" : "Phone:" + appointment.getPhone() + "\n"
//                , (appointment.getEmail() == null || appointment.getEmail().isEmpty()) ? "" : "Email:" + appointment.getEmail() + "\n"
////                ,(appointment.getNotes == null || appointment.getNotes().isEmpty()) ? "": "Notes:" + appointment.getNotes() +"\n"
//
//
//        );

        StringBuilder infoBuilder = new StringBuilder().append("")

                .append("Cita : " + event_date_str + "/ " + event_startTime_str + "-" + event_endTime_str + "\n")
//                .append((appointment.getTitle() == null || appointment.getTitle().isEmpty()) ? "" : "Name : " + appointment.getTitle() + "\n")
                .append((appointment.getPhone() == null || appointment.getPhone().isEmpty()) ? "" : "Teléfono :" + appointment.getPhone() + "\n")
                .append((appointment.getEmail() == null || appointment.getEmail().isEmpty()) ? "" : "Email :" + appointment.getEmail() + "\n")
                .append((appointment.getObservations() == null || appointment.getObservations().isEmpty()) ? "" : "Observaciones :" + appointment.getObservations() + "\n");

        if (patientTemp != null) {

            infoBuilder.
                    append(patientTemp.getReferenceName() == null || patientTemp.getReferenceName().isEmpty() ? "" : "Referencias : " + patientTemp.getReferenceName() + "\n")
                    .append(
                            patientTemp.getChildInfo() == null ? "" : patientTemp.getChildInfo().getFaterName() == null || patientTemp.getChildInfo().getFaterName().isEmpty() ? "" : "Padre: " + patientTemp.getChildInfo().getFaterName() + "\n"
                    ).append(
                    patientTemp.getChildInfo() == null ? "" : patientTemp.getChildInfo().getFatherCellPhone() == null || patientTemp.getChildInfo().getFatherCellPhone().isEmpty() ? "" : "Celular Padre: " + patientTemp.getChildInfo().getFatherCellPhone() + "\n"
            ).append(
                    patientTemp.getChildInfo() == null ? "" : patientTemp.getChildInfo().getFatherEmail() == null || patientTemp.getChildInfo().getFatherEmail().isEmpty() ? "" : "Email Padre: " + patientTemp.getChildInfo().getFatherEmail() + "\n"
            ).append(
                    patientTemp.getChildInfo() == null ? "" : patientTemp.getChildInfo().getMotherName() == null || patientTemp.getChildInfo().getMotherName().isEmpty() ? "" : "Madre: " + patientTemp.getChildInfo().getMotherName() + "\n"
            ).append(
                    patientTemp.getChildInfo() == null ? "" : patientTemp.getChildInfo().getMotherCellPhone() == null || patientTemp.getChildInfo().getMotherCellPhone().isEmpty() ? "" : "Celular Madre: " + patientTemp.getChildInfo().getMotherCellPhone() + "\n"
            )
                    .append(
                            patientTemp.getChildInfo() == null ? "" : patientTemp.getChildInfo().getMotherEmail() == null || patientTemp.getChildInfo().getMotherEmail().isEmpty() ? "" : "Email Madre: " + patientTemp.getChildInfo().getMotherEmail() + "\n"
                    )
                    .append(
                            patientTemp.getFiscalInfo() == null ? "" : patientTemp.getFiscalInfo().getSuburb() == null || patientTemp.getFiscalInfo().getSuburb().isEmpty() ? "" : "Ciudad: " + patientTemp.getFiscalInfo().getSuburb() + "\n"
                    ).append(
                    patientTemp.getFiscalInfo() == null ? "" : patientTemp.getFiscalInfo().getCounty() == null || patientTemp.getFiscalInfo().getCounty().isEmpty() ? "" : "Municipio: " + patientTemp.getFiscalInfo().getCounty() + "\n"
            )

            ;


        }


        mMaterialDialog = new MaterialDialog(getActivity())
                .setTitle(appointment.getTitle())
                .setMessage(infoBuilder.toString())
                .setCanceledOnTouchOutside(true)

                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });

        mMaterialDialog.show();
//        Toast toast = Toast.makeText(getActivity(), infoBuilder.toString(), Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.BOTTOM,(int)eventRect.width(),(int)eventRect.height());

//        toast.show();
    }


    @Override
    public void onEventDoubleClick(final WeekViewEvent event, RectF eventRect) {


        selectedStatusItem = 0;
        final Appointment appointment = event.getAppointment();

        String[] statusItems = new String[WSUtil.STATUS.values().length];
        int i = 0;
        for (WSUtil.STATUS status : WSUtil.STATUS.values()) {
            if (status.getStatus().equals(appointment.getStatus()))
                selectedStatusItem = i;
            statusItems[i++] = status.getStatus();
        }

        event_patient_str = appointment.getTitle();

        Calendar dateSelected = Calendar.getInstance();
        dateSelected.setTime(appointment.getStartDate());
        event_date_str = appointment.getStart().split(" ")[0];
        event_startTime_str = appointment.getStart().split(" ")[1];
        event_endTime_str = appointment.getEnd().split(" ")[1];
        event_phone_str = appointment.getPhone();
        event_email_str = appointment.getEmail() != null && !appointment.getEmail().isEmpty() ? appointment.getEmail() : "";
        event_notes_str = appointment.getObservations() != null && !appointment.getObservations().isEmpty() ? appointment.getObservations() : "";

        View statusView = LayoutInflater.from(getActivity()).inflate(R.layout.status_radio_group_dialog, null);
        final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity())
                .setTitle(appointment.getTitle())
                .setContentView(statusView);

        mMaterialDialog.setCanceledOnTouchOutside(true);

        RadioGroup radioGroup = (RadioGroup) statusView.findViewById(R.id.statusRadioGroupStatus);
        ((RadioButton) radioGroup.getChildAt(selectedStatusItem)).setChecked(true);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.statusRadioButtonNormal:
                        appointment.setStatus(WSUtil.STATUS.Normal.getStatus());
                        break;
                    case R.id.statusRadioButtonRetrasado:
                        appointment.setStatus(WSUtil.STATUS.Retrasado.getStatus());
                        break;
                    case R.id.statusRadioButtonLlego:
                        appointment.setStatus(WSUtil.STATUS.Llego.getStatus());
                        break;
                    case R.id.statusRadioButtonNoLlego:
                        appointment.setStatus(WSUtil.STATUS.NoLlego.getStatus());
                        break;
                    case R.id.statusRadioButtonCancelado:
                        appointment.setStatus(WSUtil.STATUS.Cancelado.getStatus());
                        break;
                    case R.id.statusRadioButtonPendiente:
                        appointment.setStatus(WSUtil.STATUS.Pendiente.getStatus());
                        break;
                    case R.id.statusRadioButtonConfirmado:
                        appointment.setStatus(WSUtil.STATUS.Confirmado.getStatus());
                        break;
                    default:
                        break;
                }
            }
        });


        final CircularProgressButton circularButtonSend = (CircularProgressButton) statusView.findViewById(R.id.statusCircularButtonSend);
        circularButtonSend.setIdleText("Actualizar");
        circularButtonSend.setIndeterminateProgressMode(true);
        circularButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.hideSoftKeyboard(getActivity());

                SendEvent(circularButtonSend, mMaterialDialog, event);


            }
        });
        final CircularProgressButton circularButtonCancel = (CircularProgressButton) statusView.findViewById(R.id.statusCircularButtonCancel);
        circularButtonCancel.setProgress(-1);
        circularButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mMaterialDialog.dismiss();


            }
        });

        mMaterialDialog.show();
    }

    @Override
    public void onEventLongPress(final WeekViewEvent event, RectF eventRect) {


//        event_date_str="";
//        event_startTime_str="";
//        event_endTime_str="";
//        event_patient_str="";
//        event_phone_str="";
//        event_email_str="";

        final Appointment appointment = event.getAppointment();

        event_patient_str = appointment.getTitle();

        Calendar dateSelected = Calendar.getInstance();
        dateSelected.setTime(appointment.getStartDate());
        event_date_str = appointment.getStart().split(" ")[0];
        event_startTime_str = appointment.getStart().split(" ")[1];
        event_endTime_str = appointment.getEnd().split(" ")[1];
        event_phone_str = appointment.getPhone();
        event_email_str = appointment.getEmail() != null && !appointment.getEmail().isEmpty() ? appointment.getEmail() : "";
        event_notes_str = appointment.getObservations() != null && !appointment.getObservations().isEmpty() ? appointment.getObservations() : "";

        View add_ev_view = LayoutInflater.from(getActivity()).inflate(R.layout.add_edit_event, null);

        Utils.hideKeyboardOnOutSideTouch(add_ev_view, getActivity());

        final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity())
                .setTitle("Cita")
                .setView(add_ev_view);
        mMaterialDialog.setCanceledOnTouchOutside(true);

        TextView event_Title_Header = (TextView) add_ev_view.findViewById(R.id.event_Title_Header);
        event_Title_Header.setText("Llenar Cita");

        final List<PatientName> patientNames = new ArrayList<>();
        final List<String> patientNamesOnly = new ArrayList<>();


//        System.out.println("WEEKVIEW " + patients + "\n");
        if (!patients.isEmpty()) {
            for (Patient patient : patients) {
                String nameAppend = new StringBuilder().append("")
                        .append(patient.getName() != null && !patient.getName().isEmpty() ? patient.getName() : "")
                        .append(patient.getName() != null && !patient.getName().isEmpty() && patient.getLastName() != null && !patient.getLastName().isEmpty() ? " " : "")
                        .append(patient.getLastName() != null && !patient.getLastName().isEmpty() ? patient.getLastName() : "")
                        .append(patient.getLastName() != null && !patient.getLastName().isEmpty() && patient.getSurName() != null && !patient.getSurName().isEmpty() ? " " : "")
                        .append(patient.getSurName() != null && !patient.getSurName().isEmpty() ? patient.getSurName() : "")
                        .toString();
                patientNamesOnly.add(nameAppend);
            }
        }

        //final EditText eventPatient = (EditText) add_ev_view.findViewById(R.id.event_patient);

        final AutoCompleteTextView eventPatient = (AutoCompleteTextView) add_ev_view.findViewById(R.id.event_patient);

        final AutoCompleteAdapter adapter = new AutoCompleteAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, (ArrayList<String>) patientNamesOnly);
        eventPatient.setAdapter(adapter);

        eventPatient.setText(event_patient_str);
        event_patient_str = eventPatient.getText().toString();
        final EditText eventPhone = (EditText) add_ev_view.findViewById(R.id.event_phoneNum);
        eventPhone.setText(event_phone_str);
        event_phone_str = eventPhone.getText().toString();

        final EditText eventEmail = (EditText) add_ev_view.findViewById(R.id.event_email);
        eventEmail.setText(event_email_str);
        event_email_str = eventEmail.getText().toString();

        final EditText eventNotes = (EditText) add_ev_view.findViewById(R.id.event_notes);
        eventNotes.setText(event_notes_str);
        event_notes_str = eventNotes.getText().toString();

        final EditText eventDatePickerTextView = (EditText) add_ev_view.findViewById(R.id.event_date);
        eventDatePickerTextView.setText(event_date_str);
        eventDatePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DialogFragment() {

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {

                        final Calendar c = Calendar.getInstance();
                        c.setTime(appointment.getStartDate());
                        int year = c.get(Calendar.YEAR);
                        int month = c.get(Calendar.MONTH);
                        int day = c.get(Calendar.DAY_OF_MONTH);
                        final DatePicker datePicker = new DatePicker(getActivity());
                        datePicker.updateDate(year, month, day);
//                        datePicker.setCalendarViewShown(false);
                        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                        if (currentapiVersion >= 11 && currentapiVersion < 21) {
                            try {
                                Method m = datePicker.getClass().getMethod("setCalendarViewShown", boolean.class);
                                m.invoke(datePicker, false);
                            } catch (Exception e) {
                            } // eat exception in our case
                        }

                        return new AlertDialog.Builder(getActivity())
                                .setTitle("Selecciona Fecha")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        // set selected date into textview
                                        event_date_str = getDateFormatAsString(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                                        eventDatePickerTextView.setText(
                                                event_date_str
                                        );

                                    }
                                })
//                                .setNegativeButton(android.R.string.cancel,
//                                        new DialogInterface.OnClickListener() {
//
//                                            @Override
//                                            public void onClick(DialogInterface dialog,
//                                                                int which) {
//                                                Log.d("Picker", "Cancelled!");
//                                            }
//                                        })
                                .setView(datePicker)
                                .create();


                        // Create a new instance of DatePickerDialog and return it
                    }

                };

                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }


        });


        final EditText eventStartTimePickerTextView = (EditText) add_ev_view.findViewById(R.id.event_start_time);
        eventStartTimePickerTextView.setText(event_startTime_str);
        eventStartTimePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DialogFragment() {

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        // Use the current time as the default values for the picker
                        final Calendar c = Calendar.getInstance();
                        c.setTime(appointment.getStartDate());
                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int minute = c.get(Calendar.MINUTE);

                        final TimePicker timePicker = new TimePicker(getActivity());
                        timePicker.setCurrentHour(hour);
                        timePicker.setCurrentMinute(minute);
                        return new AlertDialog.Builder(getActivity())
                                .setTitle("Selecciona Fecha Inicial")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        event_startTime_str = getTimeFormatAsString(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
//                                        .append(" ")

                                        eventStartTimePickerTextView.setText(
                                                event_startTime_str

                                        );

                                    }
                                })
                                .setNegativeButton(android.R.string.cancel,
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                Log.d("Picker", "Cancelled!");
                                            }
                                        }).setView(timePicker).create();


                    }


                };

                newFragment.show(getActivity().getSupportFragmentManager(), "timePickerStart");
            }
        });

        final EditText eventEndTimePickerTextView = (EditText) add_ev_view.findViewById(R.id.event_end_time);
        eventEndTimePickerTextView.setText(event_endTime_str);
        eventEndTimePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DialogFragment() {

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        // Use the current time as the default values for the picker
                        final Calendar c = Calendar.getInstance();
                        c.setTime(appointment.getEndDate());
                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int minute = c.get(Calendar.MINUTE);

                        final TimePicker timePicker = new TimePicker(getActivity());
                        timePicker.setCurrentHour(hour);
                        timePicker.setCurrentMinute(minute);

                        return new AlertDialog.Builder(getActivity())
                                .setTitle("Selecciona Hora Final")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        event_endTime_str = getTimeFormatAsString(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
//                                        .append(" ")
                                        eventEndTimePickerTextView.setText(

                                                event_endTime_str
                                        );

                                    }
                                })
//                                .setNegativeButton(android.R.string.cancel,
//                                        new DialogInterface.OnClickListener() {
//
//                                            @Override
//                                            public void onClick(DialogInterface dialog,
//                                                                int which) {
//                                                Log.d("Picker", "Cancelled!");
//                                            }
//                                        })
                                .setView(timePicker).create();
                    }


                };

                newFragment.show(getActivity().getSupportFragmentManager(), "timePickerEnd");
            }
        });

        eventPatient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


//                System.out.println("WEEKVIEW " + "On item " + position + "\n");
//                Appointment appointment = events.get(patientNames.get(position).getEventPosition()).getAppointment();
                //Appointment appointment = null;
                Patient patient = null;
                for (Patient mPatient : patients) {
                    String completeName = mPatient.getName() + " " + mPatient.getLastName() + (mPatient.getSurName() != null ? " " + mPatient.getSurName() : "");
                    if (eventPatient.getText().toString().equals(completeName)) {
                        patient = mPatient;
                        event_patient_str = completeName;
                        break;
                    }
                }
                if (patient == null)
                    return;

//                event_date_str = appointment.getStart().split(" ")[0];
//                event_startTime_str = appointment.getStart().split(" ")[1];
//                event_endTime_str = appointment.getEnd().split(" ")[1];
                event_phone_str = patient.getPhone() != null ? patient.getPhone() : "";
                event_email_str = patient.getEmail() != null && !patient.getEmail().isEmpty() ? patient.getEmail() : "";
                //event_notes_str = appointment.getObservations() != null && !appointment.getObservations().isEmpty() ? appointment.getObservations() : "";

                eventPatient.setText(event_patient_str);
//                eventDatePickerTextView.setText(event_date_str);
//                eventStartTimePickerTextView.setText(event_startTime_str);
//                eventEndTimePickerTextView.setText(event_endTime_str);
                eventPhone.setText(event_phone_str);
                eventEmail.setText(event_email_str);
                //eventNotes.setText(event_notes_str);
            }
        });


        final CircularProgressButton circularButtonCancel = (CircularProgressButton) add_ev_view.findViewById(R.id.event_circularButtonCancel);
        circularButtonCancel.setProgress(-1);
        circularButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMaterialDialog.dismiss();
            }
        });

        final CircularProgressButton circularButtonSend = (CircularProgressButton) add_ev_view.findViewById(R.id.event_circularButtonSend);
        circularButtonSend.setIdleText("Actualizar");
        circularButtonSend.setIndeterminateProgressMode(true);
        circularButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String event_date_str;
//                String event_startTime_str;
//                String event_endTime_str;
//                String event_patient_str;
//                String event_phone_str;
//                String event_email_str;

                Utils.hideSoftKeyboard(getActivity());
                event_patient_str = eventPatient.getText().toString();

                event_date_str = eventDatePickerTextView.getText().toString();
                event_startTime_str = eventStartTimePickerTextView.getText().toString();
                event_endTime_str = eventEndTimePickerTextView.getText().toString();
                event_phone_str = eventPhone.getText().toString();
                //event_email_str = eventEmail.getText().toString();
                //event_notes_str = eventNotes.getText().toString();


                if (event_date_str.isEmpty() || event_startTime_str.isEmpty() || event_endTime_str.isEmpty() || event_patient_str.isEmpty() || event_phone_str.isEmpty()) {

                    String notification = new StringBuilder("Favor de Proporcionar ")
                            .append(event_patient_str.isEmpty() ? "Nombre " : "")
                            .append(event_date_str.isEmpty() ? "Fecha " : "")
                            .append(event_startTime_str.isEmpty() ? "Hora Inicial " : "")
                            .append(event_endTime_str.isEmpty() ? "Hora Final " : "")
                            .append(event_phone_str.isEmpty() ? "Teléfono " : "")
                            .toString();
                    Toast.makeText(getActivity(), notification, Toast.LENGTH_SHORT).show();
                    return;
                } else if (WSUtil.parseDate(event_date_str + " " + event_startTime_str).getTime() > WSUtil.parseDate(event_date_str + " " + event_endTime_str).getTime()) {
                    Toast.makeText(getActivity(), "Start Time can't be larger than End Time", Toast.LENGTH_SHORT).show();
                    return;
                }


                SendEvent(circularButtonSend, mMaterialDialog, event);


            }
        });


        mMaterialDialog.show();
    }


    @Override
    public void onEmptyViewClicked(final Calendar time) {
//        Toast.makeText(getActivity(), "Empty Event @pressed event: " + time.getTime(), Toast.LENGTH_SHORT).show();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time.getTime());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        int unroundedMinutes = calendar.get(Calendar.MINUTE);
        int mod = unroundedMinutes % 15;
        calendar.add(Calendar.MINUTE, mod < 8 ? -mod : (15-mod));
//        calendar.set(Calendar.MINUTE, unroundedMinutes + mod);
        time.setTime(calendar.getTime());


        Calendar today = Calendar.getInstance();
        today.setTime(time.getTime());
        if (today.get(Calendar.DAY_OF_YEAR) > time.get(Calendar.DAY_OF_YEAR)) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.weekviewfrag_not_on_prevdate), Toast.LENGTH_SHORT).show();
            return;
        }


        int year = today.get(Calendar.YEAR);
        int month = today.get(Calendar.MONTH);
        int day = today.get(Calendar.DAY_OF_MONTH);
        event_date_str = getDateFormatAsString(year, month, day);
//        event_date_str = "";
        event_startTime_str = getTimeFormatAsString(today.get(Calendar.HOUR_OF_DAY), today.get(Calendar.MINUTE));
//        event_startTime_str = "";


        today.add(Calendar.MINUTE, 30);
        event_endTime_str = getTimeFormatAsString(today.get(Calendar.HOUR_OF_DAY), today.get(Calendar.MINUTE));
//        event_endTime_str = "";
        event_patient_str = "";
        event_phone_str = "";
        event_email_str = "";
        event_notes_str = "";


//        event_patient_str = appointment.getTitle();
//
//        Calendar dateSelected = Calendar.getInstance();
//        dateSelected.setTime(appointment.getStartDate());
//        event_date_str = appointment.getStart().split(" ")[0];
//        event_startTime_str = appointment.getStart().split(" ")[1];
//        event_endTime_str = appointment.getEnd().split(" ")[1];
//        event_phone_str = appointment.getPhone();
//        event_email_str = appointment.getEmail() != null && !appointment.getEmail().isEmpty() ? appointment.getEmail() : "";
//        event_notes_str = appointment.getObservations() != null && !appointment.getObservations().isEmpty() ? appointment.getObservations() : "";

        View add_ev_view = LayoutInflater.from(getActivity()).inflate(R.layout.add_edit_event, null);

        Utils.hideKeyboardOnOutSideTouch(add_ev_view, getActivity());

        final MaterialDialog mMaterialDialog = new MaterialDialog(getActivity())
                .setTitle(context.getString(R.string.weekviewfrag_dialog_title))
                .setView(add_ev_view);

        mMaterialDialog.setCanceledOnTouchOutside(true);

        TextView event_Title_Header = (TextView) add_ev_view.findViewById(R.id.event_Title_Header);
        event_Title_Header.setText(context.getString(R.string.weekviewfrag_dialog_evtitle));

        Appointment appointmentTemp;
        final List<PatientName> patientNames = new ArrayList<>();
        final List<String> patientNamesOnly = new ArrayList<>();


//        System.out.println("WEEKVIEW " + patients + "\n");
        if (!patients.isEmpty()) {

//            boolean loopstoper = false;
            for (Patient patient : patients) {
                String nameAppend = new StringBuilder().append("")
                        .append(patient.getName() != null && !patient.getName().isEmpty() ? patient.getName() : "")
                        .append(patient.getName() != null && !patient.getName().isEmpty() && patient.getLastName() != null && !patient.getLastName().isEmpty() ? " " : "")
                        .append(patient.getLastName() != null && !patient.getLastName().isEmpty() ? patient.getLastName() : "")
                        .append(patient.getLastName() != null && !patient.getLastName().isEmpty() && patient.getSurName() != null && !patient.getSurName().isEmpty() ? " " : "")
                        .append(patient.getSurName() != null && !patient.getSurName().isEmpty() ? patient.getSurName() : "")
                        .toString();
                patientNamesOnly.add(nameAppend);
            }
        }


        final AutoCompleteTextView eventPatient = (AutoCompleteTextView) add_ev_view.findViewById(R.id.event_patient);
//        String s[] = patientNamesOnly.toArray(new String[patientNamesOnly.size()]);

//        ArrayAdapter adapter = new ArrayAdapter
//                (getActivity(), android.R.layout.simple_dropdown_item_1line, s);
        final AutoCompleteAdapter adapter = new AutoCompleteAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line, (ArrayList<String>) patientNamesOnly);
        eventPatient.setAdapter(adapter);

//        eventPatient.setText(event_patient_str);
        event_patient_str = eventPatient.getText().toString();
        final EditText eventPhone = (EditText) add_ev_view.findViewById(R.id.event_phoneNum);
        eventPhone.setText(event_phone_str);
        event_phone_str = eventPhone.getText().toString();

        final EditText eventEmail = (EditText) add_ev_view.findViewById(R.id.event_email);
        eventEmail.setText(event_email_str);
        event_email_str = eventEmail.getText().toString();

        final EditText eventNotes = (EditText) add_ev_view.findViewById(R.id.event_notes);
        eventNotes.setText(event_notes_str);
        event_notes_str = eventNotes.getText().toString();


        final EditText eventDatePickerTextView = (EditText) add_ev_view.findViewById(R.id.event_date);
        eventDatePickerTextView.setText(event_date_str);
        eventDatePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DialogFragment() {

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {

                        final Calendar c = Calendar.getInstance();
                        c.setTime(time.getTime());
                        int year = c.get(Calendar.YEAR);
                        int month = c.get(Calendar.MONTH);
                        int day = c.get(Calendar.DAY_OF_MONTH);
                        final DatePicker datePicker = new DatePicker(getActivity());
                        datePicker.updateDate(year, month, day);
//                        datePicker.setCalendarViewShown(false);
                        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                        if (currentapiVersion >= 11 && currentapiVersion < 21) {
                            try {
                                Method m = datePicker.getClass().getMethod("setCalendarViewShown", boolean.class);
                                m.invoke(datePicker, false);
                            } catch (Exception e) {
                            } // eat exception in our case
                        }

                        return new AlertDialog.Builder(getActivity())
                                .setTitle(context.getString(R.string.weekviewfrag_dialog_date))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        // set selected date into textview
                                        event_date_str = getDateFormatAsString(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                                        eventDatePickerTextView.setText(
                                                event_date_str
                                        );

                                    }
                                })
//                                .setNegativeButton(android.R.string.cancel,
//                                        new DialogInterface.OnClickListener() {
//
//                                            @Override
//                                            public void onClick(DialogInterface dialog,
//                                                                int which) {
//                                                Log.d("Picker", "Cancelled!");
//                                            }
//                                        })
                                .setView(datePicker)
                                .create();


                        // Create a new instance of DatePickerDialog and return it
                    }

                };

                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }


        });


        final EditText eventStartTimePickerTextView = (EditText) add_ev_view.findViewById(R.id.event_start_time);
        eventStartTimePickerTextView.setText(event_startTime_str);
        eventStartTimePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DialogFragment() {

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        // Use the current time as the default values for the picker
                        final Calendar c = Calendar.getInstance();
                        c.setTime(time.getTime());
                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int minute = c.get(Calendar.MINUTE);

                        final TimePicker timePicker = new TimePicker(getActivity());
                        timePicker.setCurrentHour(hour);
                        timePicker.setCurrentMinute(minute);
                        return new AlertDialog.Builder(getActivity())
                                .setTitle(context.getString(R.string.weekviewfrag_dialog_start_time))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        event_startTime_str = getTimeFormatAsString(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
//                                        .append(" ")

                                        eventStartTimePickerTextView.setText(
                                                event_startTime_str

                                        );

                                    }
                                })
                                .setNegativeButton(android.R.string.cancel,
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                Log.d("Picker", "Cancelled!");
                                            }
                                        }).setView(timePicker).create();


                    }


                };

                newFragment.show(getActivity().getSupportFragmentManager(), "timePickerStart");
            }
        });

        final EditText eventEndTimePickerTextView = (EditText) add_ev_view.findViewById(R.id.event_end_time);
        eventEndTimePickerTextView.setText(event_endTime_str);
        eventEndTimePickerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DialogFragment() {

                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        // Use the current time as the default values for the picker
                        final Calendar c = Calendar.getInstance();
                        c.setTime(time.getTime());
                        c.add(Calendar.MINUTE, 30);
                        int hour = c.get(Calendar.HOUR_OF_DAY);
                        int minute = c.get(Calendar.MINUTE);

                        final TimePicker timePicker = new TimePicker(getActivity());
                        timePicker.setCurrentHour(hour);
                        timePicker.setCurrentMinute(minute);

                        return new AlertDialog.Builder(getActivity())
                                .setTitle(context.getString(R.string.weekviewfrag_dialog_end_time))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        event_endTime_str = getTimeFormatAsString(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
//                                        .append(" ")
                                        eventEndTimePickerTextView.setText(

                                                event_endTime_str
                                        );

                                    }
                                })
//                                .setNegativeButton(android.R.string.cancel,
//                                        new DialogInterface.OnClickListener() {
//
//                                            @Override
//                                            public void onClick(DialogInterface dialog,
//                                                                int which) {
//                                                Log.d("Picker", "Cancelled!");
//                                            }
//                                        })
                                .setView(timePicker).create();
                    }


                };

                newFragment.show(getActivity().getSupportFragmentManager(), "timePickerEnd");
            }
        });


        final CircularProgressButton circularButtonCancel = (CircularProgressButton) add_ev_view.findViewById(R.id.event_circularButtonCancel);
        circularButtonCancel.setProgress(-1);
        circularButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMaterialDialog.dismiss();
            }
        });


//        System.out.println("WEEKVIEW " + "On ItemClieck" + "\n");
//        eventPatient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
        eventPatient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


//                System.out.println("WEEKVIEW " + "On item " + position + "\n");
//                Appointment appointment = events.get(patientNames.get(position).getEventPosition()).getAppointment();
                //Appointment appointment = null;
                Patient patient = null;
                for (Patient mPatient : patients) {
                    String completeName = mPatient.getName() + " " + mPatient.getLastName() + (mPatient.getSurName()!=null?" " + mPatient.getSurName():"");
                    if (eventPatient.getText().toString().equals(completeName)) {
                        patient = mPatient;
                        event_patient_str = completeName;
                        break;
                    }
                }
                if (patient == null)
                    return;

//                event_date_str = appointment.getStart().split(" ")[0];
//                event_startTime_str = appointment.getStart().split(" ")[1];
//                event_endTime_str = appointment.getEnd().split(" ")[1];
                event_phone_str = patient.getPhone() != null ? patient.getPhone() : "";
                event_email_str = patient.getEmail() != null && !patient.getEmail().isEmpty() ? patient.getEmail() : "";
                //event_notes_str = appointment.getObservations() != null && !appointment.getObservations().isEmpty() ? appointment.getObservations() : "";

                eventPatient.setText(event_patient_str);
//                eventDatePickerTextView.setText(event_date_str);
//                eventStartTimePickerTextView.setText(event_startTime_str);
//                eventEndTimePickerTextView.setText(event_endTime_str);
                eventPhone.setText(event_phone_str);
                eventEmail.setText(event_email_str);
                //eventNotes.setText(event_notes_str);
            }
        });


        final CircularProgressButton circularButtonSend = (CircularProgressButton) add_ev_view.findViewById(R.id.event_circularButtonSend);
        circularButtonSend.setIndeterminateProgressMode(true);
        circularButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String event_date_str;
//                String event_startTime_str;
//                String event_endTime_str;
//                String event_patient_str;
//                String event_phone_str;
//                String event_email_str;

                Utils.hideSoftKeyboard(getActivity());
                event_patient_str = eventPatient.getText().toString();

                event_date_str = eventDatePickerTextView.getText().toString();
                event_startTime_str = eventStartTimePickerTextView.getText().toString();
                event_endTime_str = eventEndTimePickerTextView.getText().toString();
                event_phone_str = eventPhone.getText().toString();
                event_email_str = eventEmail.getText().toString();
                event_notes_str = eventNotes.getText().toString();


                if (event_date_str.isEmpty() || event_startTime_str.isEmpty() || event_endTime_str.isEmpty() || event_patient_str.isEmpty() || event_phone_str.isEmpty()) {

                    String notification = new StringBuilder(context.getString(R.string.weekviewfrag_input_form))
                            .append(event_patient_str.isEmpty() ? context.getString(R.string.weekviewfrag_input_form_name) : "")
                            .append(event_date_str.isEmpty() ? context.getString(R.string.weekviewfrag_input_form_date) : "")
                            .append(event_startTime_str.isEmpty() ? context.getString(R.string.weekviewfrag_input_form_start_time) : "")
                            .append(event_endTime_str.isEmpty() ? context.getString(R.string.weekviewfrag_input_form_end_time) : "")
                            .append(event_phone_str.isEmpty() ? context.getString(R.string.weekviewfrag_input_form_phone) : "")
                            .toString();
                    Toast.makeText(getActivity(), notification, Toast.LENGTH_SHORT).show();
                    return;
                } else if (WSUtil.parseDate(event_date_str + " " + event_startTime_str).getTime() > WSUtil.parseDate(event_date_str + " " + event_endTime_str).getTime()) {
                    Toast.makeText(getActivity(), context.getString(R.string.weekviewfrag_input_form_starttime_cant_less), Toast.LENGTH_SHORT).show();
                    return;
                }


                SendEvent(circularButtonSend, mMaterialDialog, null);


            }
        });


        mMaterialDialog.show();
    }


    private void SendEvent(final CircularProgressButton circularButtonSend, final MaterialDialog mMaterialDialog, final WeekViewEvent event) {

        mMaterialDialog.setCanceledOnTouchOutside(false);
        circularButtonSend.setProgress(50);


        Appointment appointment = (event != null) ? event.getAppointment() : new Appointment();

        Patient patient = null;
        for (Patient mPatient : patients) {
            String completeName = mPatient.getName() + " " + mPatient.getLastName() + (mPatient.getSurName()!=null?" " + mPatient.getSurName():"");
            if (event_patient_str.equals(completeName)) {
                patient = mPatient;
                event_patient_str = completeName;
                break;
            }
        }


        appointment.setType("A");
        appointment.setAppointmentId(appointment.getAppointmentId() != null && !appointment.getAppointmentId().isEmpty() ? appointment.getAppointmentId() : "");
        if (patient != null)
            appointment.setClientId(patient.getClientId());
        appointment.setAllDay(false);

        appointment.setTitle(event_patient_str);

        appointment.setStart(event_date_str + " " + event_startTime_str);
        appointment.setStartDate(WSUtil.parseDate(event_date_str + " " + event_startTime_str));

        appointment.setEnd(event_date_str + " " + event_endTime_str);
        appointment.setEndDate(WSUtil.parseDate(event_date_str + " " + event_endTime_str));

        appointment.setStatus(appointment.getStatus() != null && !appointment.getStatus().isEmpty() ? appointment.getStatus() : context.getString(R.string.appointment_default_status));

        appointment.setEmail(event_email_str);

        appointment.setPhone(event_phone_str);

        appointment.setObservations(event_notes_str);


        final int NUMBER_OF_TASK = 2;

        final MyAsyncTask asyncTask = new MyAsyncTask(getActivity(), ((NUMBER_OF_TASK * 2) + 2)) {
            @Override
            protected void updateUI(List<Object> responseAsObjects) {


                if (responseAsObjects != null && !responseAsObjects.isEmpty()) {

                    DoctorSearchAllPatientsResponse doctorSearchAllPatientsResponse;
                    SaveEventResponse saveEventResponse = null;
                    for (int i = 0; i < responseAsObjects.size(); i++) {
                        Object responseAsObject = responseAsObjects.get(i);
//                    if (responseAsObject != null) {

                        if (responseAsObject instanceof DoctorSearchAllPatientsResponse) {
                            doctorSearchAllPatientsResponse = ((DoctorSearchAllPatientsResponse) responseAsObject);

                            if (!patients.isEmpty()) patients.clear();
                            patients.addAll(doctorSearchAllPatientsResponse.getSearchAllPatientsResponse().getPatients());


//                        tv.setText(responseAsObject != null ? doctorInfoResponse.toString() : "@null");
                        } else if (responseAsObject instanceof AppointmentResponse) {
//                            doctorAppointmentsResponse = ((AppointmentResponse) responseAsObject).getDoctorAppointmentsResponse();
//                        tv.setText(responseAsObject != null ? doctorAppointmentsResponse.toString() : "@null");

//                            addEvents(doctorAppointmentsResponse.getAppointments());
                        } else if (responseAsObject instanceof DoctorSaveEventResponse) {
                            saveEventResponse = ((DoctorSaveEventResponse) responseAsObject).getSaveEventResponse();


                            Appointment appointmentIn = saveEventResponse.getEvents().get(0);
                            appointmentIn.setStartDate(WSUtil.parseDate(appointmentIn.getStart()));
                            appointmentIn.setEndDate(WSUtil.parseDate(appointmentIn.getEnd()));
                            Calendar dateStartIn = Calendar.getInstance();
                            Calendar dateEndIn = (Calendar) dateStartIn.clone();
                            dateStartIn.setTime(appointmentIn.getStartDate());
                            dateEndIn.setTime(appointmentIn.getEndDate());

                            WeekViewEvent eventIn = (event != null) ? event : new WeekViewEvent();
                            eventIn.setName(getEventTitle(appointmentIn, dateStartIn, dateEndIn));
                            eventIn.setStartTime(dateStartIn);
                            eventIn.setEndTime(dateEndIn);
                            eventIn.setColor(Color.parseColor(WSUtil.getColorAsString(appointmentIn.getStatus())));
                            eventIn.setAppointment(appointmentIn);

                            eventIn.setId((event != null) ? event.getId() : events.size() + 1);

                            if (event == null) {
                                events.add(eventIn);
                            } else if (appointmentIn.getStatus().equals("Cancelado")){
                                events.remove(event);
                            }
                        }
                    }


//                        event = new WeekViewEvent(i + 1, getEventTitle(appointment, startTime, endTime), startTime, endTime);
//                        event.setColor(Color.parseColor(WSUtil.getColorAsString(appointment.getStatus())));
//                        event.setAppointment(appointment);
//                        events.add(event);


//                    } else {
//                        circularButtonSend.setProgress(-1);
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                circularButtonSend.setProgress(0);
//                                mMaterialDialog.setCanceledOnTouchOutside(true);
//                            }
//                        }, 1000);
//                        Toast.makeText(this.context, "Error Inside some param @null", Toast.LENGTH_LONG).show();
//                    }


                    circularButtonSend.setProgress(100);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mMaterialDialog.dismiss();
                            mWeekView.notifyDatasetChanged();
                        }
                    }, 1000);

                } else {
                    circularButtonSend.setProgress(-1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            circularButtonSend.setProgress(0);
                            mMaterialDialog.setCanceledOnTouchOutside(true);
                        }
                    }, 1000);
                    Toast.makeText(this.context, context.getString(R.string.failed_retry), Toast.LENGTH_LONG).show();
                }

            }
        };

        asyncTask.execute(URLs.get_PATIENTS_EVENT_JSon(appointment), URLs.getCommandJson(CMD.PATIENTS_INFO_GET, null));


    }


    private String getDateFormatAsString(int year, int monthOfYear, int dayOfMonth) {

        return new StringBuilder()
                .append(year)
                .append("-")
                .append(monthOfYear + 1 < 10 ? "0" + (monthOfYear + 1) : (monthOfYear + 1))
                .append("-")
                .append(dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth)
                .toString();
    }

    private String getTimeFormatAsString(int hourOfDay, int minute) {

        return new StringBuilder()
                .append(hourOfDay)
                .append(":")
                .append(minute < 10 ? "0" + minute : minute)
                .toString();
    }

    abstract public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }


    }

    abstract public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }


    }


}


