package me.rafsun.mydoctor.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import me.drakeet.materialdialog.MaterialDialog;
import me.rafsun.mydoctor.R;
import me.rafsun.mydoctor.Singletones.DoctorInfoSingletone;
import me.rafsun.mydoctor.Singletones.PatientInfoSingletone;
import me.rafsun.mydoctor.Singletones.WeekEventSingletone;
import me.rafsun.mydoctor.Utils.Utils;
import me.rafsun.mydoctor.json.Doctor;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {


    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private MaterialDialog materialDialog;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Calendar fixedDate = Calendar.getInstance();
//        fixedDate.set(2015, 2, 15);
//        if (Calendar.getInstance().getTimeInMillis() > fixedDate.getTimeInMillis())
//            this.finish();
        setContentView(R.layout.activity_main);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        if (position == 0) {
            WeekViewFragment weekViewFragment = WeekViewFragment.newInstance(position + 1);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.replace(R.id.container, weekViewFragment)
                    .commit();
        } else if (position == 1) {

            Doctor doctor = DoctorInfoSingletone.INSTANCE.getInstance().getDoctorInfoResponse() != null ? DoctorInfoSingletone.INSTANCE.getInstance().getDoctorInfoResponse().getDoctor() : null;
            if (doctor != null) {
                String nameAppend = new StringBuilder().append("")
                        .append(doctor.getName() != null && !doctor.getName().isEmpty() ? getString(R.string.doctor_info_name) + doctor.getName() + "\n" : "")
                        .append(doctor.getSpeciality() != null && !doctor.getSpeciality().isEmpty() ? getString(R.string.doctor_info_speciality) + doctor.getSpeciality() + "\n" : "")
                        .append(doctor.getEmailPersonal() != null && !doctor.getEmailPersonal().isEmpty() ? getString(R.string.doctor_info_email_personal) + doctor.getEmailPersonal() + "\n" : "")
                        .append(doctor.getEmailOffice() != null && !doctor.getEmailOffice().isEmpty() ? getString(R.string.doctor_info_email_office) + doctor.getEmailOffice() + "\n" : "")
                        .append(doctor.getPhoneMobile() != null && !doctor.getPhoneMobile().isEmpty() ? getString(R.string.doctor_info_phone_mobile) + doctor.getPhoneMobile() + "\n" : "")
                        .append(doctor.getPhoneHome() != null && !doctor.getPhoneHome().isEmpty() ? getString(R.string.doctor_info_phone_home) + doctor.getPhoneHome() + "\n" : "")
                        .append(doctor.getPhoneOffice() != null && !doctor.getPhoneOffice().isEmpty() ? getString(R.string.doctor_info_phone_office) + doctor.getPhoneOffice() + "\n" : "")
                        .append(doctor.getRenewwalDate() != null && !doctor.getRenewwalDate().isEmpty() ? getString(R.string.doctor_info_renewal_office) + doctor.getRenewwalDate() + "\n" : "")

                        .toString();

                materialDialog = new MaterialDialog(this)
                        .setTitle(getString(R.string.title_doctor_info))
                        .setMessage(nameAppend)
                        .setCanceledOnTouchOutside(true)
                        .setPositiveButton(android.R.string.ok,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                            }
                        });
                materialDialog.show();
            }else{
                materialDialog = new MaterialDialog(this)
                        .setTitle(getString(R.string.title_doctor_info))
                        .setMessage(getString(R.string.message_doctor_info))
                        .setCanceledOnTouchOutside(true)
                        .setPositiveButton(android.R.string.ok,new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                materialDialog.dismiss();
                            }
                        });
                materialDialog.show();
            }
        } else if (position == 2) {

            SharedPreferences editor = this.getSharedPreferences(Utils.PREF_KEY_LOG, Context.MODE_PRIVATE);
            editor.edit().remove(Utils.PREF_USER).apply();
            editor.edit().remove(Utils.PREF_PASS).apply();

            PatientInfoSingletone.INSTANCE.getInstance().clear();
            WeekEventSingletone.INSTANCE.getInstance().clear();

            Toast.makeText(this, getString(R.string.message_logging_out), Toast.LENGTH_SHORT).show();

            startActivity(new Intent(this, LoginActivity.class));
            this.finish();

//            LoginFragment loginFragment = LoginFragment.getNewInstance(1);
//
//            FragmentManager fragmentManager = getSupportFragmentManager();
//
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//
//            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
//            fragmentTransaction.replace(R.id.container, loginFragment)
//                    .commit();
        }
    }

    public void onSectionAttached(int number) {

        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
