package me.rafsun.mydoctor.ui;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.dd.CircularProgressButton;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import me.rafsun.mydoctor.MyWeekView.WeekViewEvent;
import me.rafsun.mydoctor.R;
import me.rafsun.mydoctor.Singletones.DoctorInfoSingletone;
import me.rafsun.mydoctor.Singletones.PatientInfoSingletone;
import me.rafsun.mydoctor.Singletones.WeekEventSingletone;
import me.rafsun.mydoctor.Utils.CMD;
import me.rafsun.mydoctor.Utils.URLs;
import me.rafsun.mydoctor.Utils.Utils;
import me.rafsun.mydoctor.Utils.WSUtil;
import me.rafsun.mydoctor.json.Appointment;
import me.rafsun.mydoctor.json.AppointmentResponse;
import me.rafsun.mydoctor.json.DoctorAppointmentsResponse;
import me.rafsun.mydoctor.json.DoctorInfoResponse;
import me.rafsun.mydoctor.json.DoctorSearchAllPatientsResponse;
import me.rafsun.mydoctor.json.InfoResponse;
import me.rafsun.mydoctor.json.Patient;

/**
 * Created by Rafsun on 3/6/15.
 */
public class LoginFragment extends Fragment {

    private static final String KEY_ACC = "FRAG_BUNDLE";
    private int curr_param_int = -1;

    private CheckBox checkBox;
    public LoginFragment() {

    }

    public static LoginFragment getNewInstance(int i) {
//        LoginFragment.mContext = mContext;

        LoginFragment fragment = new LoginFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ACC, i);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        curr_param_int = getArguments() != null ? getArguments().getInt(KEY_ACC) : -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.login_fragment, container, false);


        final EditText loginUsername = (EditText) rootView.findViewById(R.id.login_username);
        final EditText loginPassword = (EditText) rootView.findViewById(R.id.login_password);
        checkBox = (CheckBox) rootView.findViewById(R.id.login_checkBox);


        final CircularProgressButton loginButton = (CircularProgressButton) rootView.findViewById(R.id.login_button);
        loginButton.setIndeterminateProgressMode(true);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {


                String username = loginUsername.getText().toString();
                String password = loginPassword.getText().toString();
                if (username.isEmpty() && password.isEmpty()) {
                    String notification = new StringBuilder("Please Enter ")
                            .append(username.isEmpty() ? "Username " : "")
                            .append(password.isEmpty() ? "Password " : "")
                            .toString();
                    Toast.makeText(getActivity(), notification, Toast.LENGTH_SHORT).show();
                    return;
                }
//

                login(button, username, password);
            }
        });


        Utils.hideKeyboardOnOutSideTouch(rootView, getActivity());
        return rootView;
    }


    private void login(View mButton, final String username, final String password) {


        Utils.hideSoftKeyboard(getActivity());
        URLs.setUser(username, password);

        final CircularProgressButton loginButton = (CircularProgressButton) mButton;
        loginButton.setProgress(50);

        final int NUMBER_OF_TASK = 3;

        final MyAsyncTask asyncTask = new MyAsyncTask(getActivity(), ((NUMBER_OF_TASK * 2) + 2)) {
            @Override
            protected void updateUI(List<Object> responseAsObjects) {

                List<Patient> patients = PatientInfoSingletone.INSTANCE.getInstance();

                if (responseAsObjects != null && !responseAsObjects.isEmpty()) {

                    DoctorInfoResponse doctorInfoResponse;
                    DoctorAppointmentsResponse doctorAppointmentsResponse;
                    DoctorSearchAllPatientsResponse doctorSearchAllPatientsResponse;
                    for (int i = 0; i < responseAsObjects.size(); i++) {
                        Object responseAsObject = responseAsObjects.get(i);

                        if (responseAsObject != null) {


                            if (responseAsObject instanceof DoctorSearchAllPatientsResponse) {
                                doctorSearchAllPatientsResponse = ((DoctorSearchAllPatientsResponse) responseAsObject);

                                if (!patients.isEmpty()) patients.clear();
                                patients.addAll(doctorSearchAllPatientsResponse.getSearchAllPatientsResponse().getPatients());


//                        tv.setText(responseAsObject != null ? doctorInfoResponse.toString() : "@null");
                            } else if (responseAsObject instanceof InfoResponse) {
                                doctorInfoResponse = ((InfoResponse) responseAsObject).getDoctorInfoResponse();
                                DoctorInfoSingletone.INSTANCE.getInstance().setDoctorInfoResponse(doctorInfoResponse);
//                        tv.setText(responseAsObject != null ? doctorInfoResponse.toString() : "@null");
                            } else if (responseAsObject instanceof AppointmentResponse) {
                                doctorAppointmentsResponse = ((AppointmentResponse) responseAsObject).getDoctorAppointmentsResponse();
//                        tv.setText(responseAsObject != null ? doctorAppointmentsResponse.toString() : "@null");

                                addEvents(doctorAppointmentsResponse.getAppointments());
                            }


                        }
//                    else {
//                        loginButton.setProgress(-1);
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                loginButton.setProgress(0);
//                            }
//                        }, 1000);
//                        Toast.makeText(this.context, "Error Inside some param @null", Toast.LENGTH_LONG).show();
//                    }
                    }

                    if(checkBox.isChecked()){
                        saveAtSharedPref(username, Utils.sha256Hex(password));
                    }

                    loginButton.setProgress(100);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.gotoNextActivity(getActivity());
                        }
                    }, 1000);


                } else {
                    loginButton.setProgress(-1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loginButton.setProgress(0);
                        }
                    }, 1000);
                    Toast.makeText(this.context, "@null", Toast.LENGTH_LONG).show();
                }


            }
        };

        asyncTask.execute(URLs.getCommandJson(CMD.PATIENTS_APPOINTMENT_GET, null), URLs.getCommandJson(CMD.PATIENTS_INFO_GET, null),URLs.getCommandJson(CMD.DOCTOR_INFO_GET,null));


    }





    private boolean saveAtSharedPref(String username, String shaPassword) {

        SharedPreferences.Editor editor = getActivity().getSharedPreferences(Utils.PREF_KEY_LOG, Context.MODE_PRIVATE).edit();
        editor.putString(Utils.PREF_USER,username);
        editor.putString(Utils.PREF_PASS,shaPassword);

        return editor.commit();

    }



    private void addEvents(List<Appointment> appointments) {

        List<WeekViewEvent> events = WeekEventSingletone.INSTANCE.getInstance();

        if (!events.isEmpty()) events.clear();

        WeekViewEvent event;
        Calendar startTime;

        Calendar endTime;


        for (int i = 0; i < appointments.size(); i++) {
            Appointment appointment = appointments.get(i);

            //only for appointment type
            if (appointment.getType().equals("A") && !appointment.getStatus().equals(WSUtil.STATUS.Cancelado.getStatus())) {
                startTime = Calendar.getInstance();
                endTime = (Calendar) startTime.clone();
                Date dateStartDate = appointment.getStartDate();
                Date dateEndDate = appointment.getEndDate();
                startTime.setTime(dateStartDate);
                endTime.setTime(dateEndDate);


                event = new WeekViewEvent(i + 1, WeekViewFragment.getEventTitle(appointment, startTime, endTime), startTime, endTime);
                event.setColor(Color.parseColor(WSUtil.getColorAsString(appointment.getStatus())));
                event.setAppointment(appointment);
                events.add(event);
            }
        }


    }

}
