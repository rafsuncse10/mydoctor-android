package me.rafsun.mydoctor.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;

import me.rafsun.mydoctor.R;
import me.rafsun.mydoctor.Utils.Utils;

/**
 * Created by Rafsun on 3/6/15.
 */
public class LoginActivity extends ActionBarActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);


        if (savedInstanceState == null) {

            if (!Utils.isSavedAtSharedPref(this)) {
                LoginFragment loginFragment = LoginFragment.getNewInstance(1);

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .add(R.id.login_container, loginFragment)
                        .commit();
            } else {
                Utils.gotoNextActivity(LoginActivity.this);
            }
        }

//        setTitle("Log In");

    }





}
