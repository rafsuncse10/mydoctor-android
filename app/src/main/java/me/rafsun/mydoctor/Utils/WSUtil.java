package me.rafsun.mydoctor.Utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rafsun on 2/25/15.
 */
public class WSUtil {

    public static Date parseDate(String date) {
        SimpleDateFormat dateFormat = null;
        Date ds = null;

        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            ds = dateFormat.parse(date);
        } catch (ParseException ex) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                ds = dateFormat.parse(date);
            } catch (ParseException ex1) {
                dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                try {
                    ds = dateFormat.parse(date);
                } catch (ParseException ex2) {
                    dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        ds = dateFormat.parse(date);
                    } catch (ParseException ex3) {
                        ds = null;
                    }
                }
            }
        }

        return ds;
    }

    public static JsonArray getDoctorAppointments(JsonElement jdata) {
        if (!isDoctorAppointmentsResponse(jdata)) {
            return null;
        }
        return jdata.getAsJsonObject().get("doctorAppointmentsResponse").getAsJsonObject().get("appointments").getAsJsonArray();

    }

    public static boolean isExceptionFromServer(JsonElement jdata) {
        return (jdata.getAsJsonObject().get("exception") != null) ? true : false;
    }

    public static boolean isDoctorAppointmentsResponse(JsonElement jdata) {
        if (isReceived(jdata)) {
            return false;
        }

        return (jdata.getAsJsonObject().get("doctorAppointmentsResponse") != null) ? true : false;
    }

    public static boolean isDoctorInfoResponse(JsonElement jdata) {
        if (isReceived(jdata)) {
            return false;
        }

        return (jdata.getAsJsonObject().get("doctorInfoResponse") != null) ? true : false;
    }


    public static boolean isSaveEventResponse(JsonElement jdata) {
        if (isReceived(jdata)) {
            return false;
        }

        return (jdata.getAsJsonObject().get("saveEventResponse") != null) ? true : false;
    }

    public static boolean isSearchAllPatientsResponse(JsonElement jdata) {
        if (isReceived(jdata)) {
            return false;
        }

        return (jdata.getAsJsonObject().get("searchAllPatientsResponse") != null) ? true : false;
    }

    public static boolean isLoginSuccessful(JsonElement jdata) {
        if (isLoginResponse(jdata)) {
            return (jdata.getAsJsonObject().get("loginResponse").getAsJsonObject().get("message").getAsString().equals("successful")) ? true : false;
        }
        return false;

    }

    public static boolean isLoginResponse(JsonElement jdata) {
        if (isReceived(jdata)) {
            return false;
        }

        return (jdata.getAsJsonObject().get("loginResponse") != null) ? true : false;

    }

    public static boolean isReceived(JsonElement jdata) {

        return (jdata.getAsJsonObject().get("received") != null) ? true : false;

    }

    public static boolean isToken(JsonElement jdata) {

        return (jdata.getAsJsonObject().get("token") != null) ?
                true
                : false;

    }

    public static boolean isTokenPositive(JsonElement jdata) {

        return (jdata.getAsJsonObject().get("token") != null) ?
                jdata.getAsJsonObject().get("token").getAsInt() != -1 ? true : false
                : false;

    }


    public static String getColorAsString(String statusName) {

        if (statusName == null)
            return "#42000000";

        if (statusName.equals(STATUS.Normal.getStatus()))
            return "#6495ED";

        else if (statusName.equals(STATUS.Retrasado.getStatus()))
            return "#f8b552";

        else if (statusName.equals(STATUS.Llego.getStatus()))
            return "#87d288";

        else if (statusName.equals(STATUS.NoLlego.getStatus()))
            return "#f57f68";

        else if (statusName.equals(STATUS.Cancelado.getStatus()))
            return "#8a000000";

        else if (statusName.equals(STATUS.Pendiente.getStatus()))
            return "#B0AFE3";

        else if (statusName.equals(STATUS.Confirmado.getStatus()))
            return "#FFC0CB";
        else
            //return if no match
            return "#33999999";

    }

//
//    public static String getColorAsString(String statusName){
//
//        if(statusName == null)
//            return "#FFFFFF";
//
//        if(statusName.equals("Normal"))
//            return "#6495ED";
//
//        else if (statusName.equals("Retrasado"))
//            return "#FF8C00";
//
//        else if (statusName.equals("Llego"))
//            return "#32CD32";
//
//        else if (statusName.equals("No llego"))
//            return "#EE0000";
//
//        else if (statusName.equals("Cancelado"))
//            return "#000000";
//
//        else if (statusName.equals("Pendiente"))
//            return "#B0AFE3";
//
//        else if (statusName.equals("Confirmado"))
//            return "#FFC0CB";
//        else
//            //return if no match
//            return "#FFFFFF";
//
//    }
//


    public enum STATUS {

        Normal("Normal"), Retrasado("Retrasado"), Llego("Llego"), NoLlego("No llego"), Cancelado("Cancelado"), Pendiente("Pendiente"), Confirmado("Confirmado");


        private String stat;

        private STATUS(String stat) {
            this.stat = stat;
        }

        public String getStatus() {
            return this.stat;
        }
    }


}
