package me.rafsun.mydoctor.Utils;

import java.util.Calendar;
import java.util.List;

import me.rafsun.mydoctor.json.Appointment;

/**
 * Created by Rafsun on 2/27/15.
 */
public class URLs {

//    public static final String URI_ROOT = "ws://104.239.134.74:8080/nuvex-medica-war/dashboard";
//
//    //        Draft
//    public static String EMAIL = "hlp@keynet.com.mx";
//    public static String PASSOWORD = "1d3c5s";
//      public static String EMAIL = "juan.salazar@keynet.com.mx";
//    public static String PASSOWORD = "m4t3055";
//
//    //    private static String PASSWORD_SHA_256 = org.apache.commons.codec.digest.DigestUtils.sha256Hex(PASSOWORD);
//    public static String PASSWORD_SHA_256 = WSUtil.sha256Hex(PASSOWORD);
//
//    public static String SENT_MESSAGE_LOGIN = "{\"command\":\"login\",\"token\":" + "1" + ",\"email\":\"" + EMAIL + "\",\"password\":\"" + PASSWORD_SHA_256 + "\"}";
//    public static String SENT_MESSAGE_DOCTOR_INFO = "{\"command\":\"getDoctorInfo\",\"token\":" + "2" + ",\"email\":\"" + EMAIL + "\"}";
//    public static String SENT_GET_ALL_APPOINTMENTS = "{\"command\":\"getDoctorAppointments\",\"token\":" + "3" + ",\"doctorId\":\"" + EMAIL
//            + "\",\"startMonth\":" + 2
//            + ",\"endMonth\":" + 3
//            + ",\"startYear\":" + 2015
//            + ",\"endYear\":" + 2015 + "}";


    private static int token = 0;
    private static String EMAIL ;
//    private static String EMAIL = "hlp@keynet.com.mx";
    private static String PASSWORD;
//    private static String PASSWORD = WSUtil.sha256Hex("1d3c5s");
    //in seconds
    public static int TIME_OUT = 25 ;

    public static String getRootURL() {

        return "ws://docdoc.mx:8080/nuvex-medica-war/dashboard";
    }

    public static void setUser(String Email, String Password) {
        EMAIL = Email;
        PASSWORD = Utils.sha256Hex(Password);
    }

    public static void setUsername(String username){
        EMAIL = username;
    }
    public static void setSHAPassword(String shaPassword){
        PASSWORD =  shaPassword;
    }

    public static String getUserEmail() {

        return EMAIL;
    }

    public static String getUserPassword() {
        return PASSWORD;
    }

    //    private static String PASSWORD_SHA_256 = sha256Hex(PASSOWORD);
    public static String getCommandJson(CMD Command, List<String> CMDParams) {
        switch (Command) {
            case LOGIN:
                //Set UserName n Password
                //Remove bellow after Testing
                //setUser(CMDParams.get(0), CMDParams.get(1));

                return "{\"command\":\"login\",\"token\":"
                        + getToken()
                        + ",\"email\":\""
                        + getUserEmail()
                        + "\",\"password\":\""
                        + getUserPassword()
                        + "\"}";
            case DOCTOR_INFO_GET:
                return "{\"command\":\"getDoctorInfo\",\"token\":"
                        + getToken()
                        + ",\"email\":\""
                        + getUserEmail()
                        + "\"}";

            case PATIENTS_INFO_GET:
                return "{\"command\":\"searchAllPatients\",\"token\":"
                        + getToken()
                        + "}";

            case PATIENTS_APPOINTMENT_GET:
                //Set Times
                //Remove bellow after Testing
                //setUser(CMDParams.get(0), CMDParams.get(1));
                int startMonth = Calendar.getInstance().get(Calendar.MONTH)+1-1;
//                int startMonth = Integer.parseInt(CMDParams.get(0)) ;
                int endMonth = Calendar.getInstance().get(Calendar.MONTH)+1+1;
//                int endMonth = Integer.parseInt(CMDParams.get(1)) ;
                int startYear = 2015;
//                int startYear = Integer.parseInt(CMDParams.get(2)) ;
                int endYear = 2015;
//                int endYear = Integer.parseInt(CMDParams.get(3)) ;
                return "{\"command\":\"getDoctorAppointments\",\"token\":"
                        + getToken()
                        + ",\"doctorId\":\""
                        + getUserEmail()
                        + "\",\"startMonth\":" + startMonth
                        + ",\"endMonth\":" + endMonth
                        + ",\"startYear\":" + startYear
                        + ",\"endYear\":" + endYear
                        + "}";

            case PATIENTS_EVENT_SET:
                //Set params using appointment
                //return CMDParams.get(0)

                return CMDParams.get(0);
            case PATIENTS_EVENT_UPDATE:
                //Set params using appointment
                //return CMDParams.get(0)

                return CMDParams.get(0);

            default:
                return "";
        }
    }

    public static int getToken() {
        return ++token;
    }

    public static String get_PATIENTS_EVENT_JSon(Appointment appointment) {
        String ClientId = Integer.toString(appointment.getClientId());

        if (ClientId.equals("0")) {
            ClientId = "";
        }
        StringBuilder builder = new StringBuilder();


        builder.append("{")
                .append("\"command\":\"saveEvent\",\"token\":")
                .append(getToken())
                .append(",\"events\":")
                .append("[")
                .append("{")
                .append(appointment.getAppointmentId() != null && !appointment.getAppointmentId().isEmpty() ? "\"appointmentId\":\"" + appointment.getAppointmentId() + "\"," : "")
                .append(appointment.getType() != null && !appointment.getType().isEmpty() ? "\"type\":\"" + appointment.getType() + "\"," : "")
                .append(appointment.getStart() != null && !appointment.getStart().isEmpty() ? "\"start\":\"" + appointment.getStart() + "\"," : "")
                .append(appointment.getEnd() != null && !appointment.getEnd().isEmpty() ? "\"end\":\"" + appointment.getEnd() + "\"," : "")
                .append("\"allDay\":" + appointment.isAllDay() + ",")
                .append(appointment.getTitle() != null && !appointment.getTitle().isEmpty() ? "\"title\":\"" + appointment.getTitle() + "\"," : "")
                .append(appointment.getStatus() != null && !appointment.getStatus().isEmpty() ? "\"status\":\"" + appointment.getStatus() + "\"," : "")
                .append(appointment.getEmail() != null && !appointment.getEmail().isEmpty() ? "\"email\":\"" + appointment.getEmail() + "\"," : "")
                .append(appointment.getPhone() != null && !appointment.getPhone().isEmpty() ? "\"phone\":\"" + appointment.getPhone() + "\"," : "")
                .append(ClientId != null && !ClientId.isEmpty() ? "\"clientId\":" + ClientId + "," : "")
                .append(appointment.getObservations() != null && !appointment.getObservations().isEmpty() ? "\"observations\":\"" + appointment.getObservations() + "\"," : "")
                .append(appointment.getLastUserId() != null && !appointment.getLastUserId().isEmpty() ? "\"lastUserId\":\"" + appointment.getLastUserId() + "\"," : "")
                .append("\"updated\":false,")
                .append("\"saved\":false")
                .append("}")
                .append("]")
                .append("}");

        return builder.toString();
    }

//    private static String SENT_MESSAGE_LOGIN = "{\"command\":\"login\",\"token\":" + "1" + ",\"email\":\"" + EMAIL + "\",\"password\":\"" + PASSWORD_SHA_256 + "\"}";
//    private static String SENT_MESSAGE_DOCTOR_INFO = "{\"command\":\"getDoctorInfo\",\"token\":" + "3" + ",\"email\":\"" + EMAIL + "\"}";
//    private static String SENT_GET_ALL_APPOINTMENTS = "{\"command\":\"getDoctorAppointments\",\"token\":" + "3" + ",\"doctorId\":\"" + EMAIL
//            + "\",\"startMonth\":" + 2
//            + ",\"endMonth\":" + 3
//            + ",\"startYear\":" + 2015
//            + ",\"endYear\":" + 2015 + "}";
//    private static String SENT_GET_ALL_PATIENT_INFO = "{\"command\":\"searchAllPatients\",\"token\":" + "2" + "}";
//    private static String SENT_GET_SAVE_UPDATE_APPOINTMENTS
//            = //            "{" +
//            "appointmentId\":\"hlp@keynet.com.mx-02-2015-00261\","
//            + "type\":\"A\","
//            + "start\":\"2015-02-19 15:30\","
//            + "end\":\"2015-02-19 16:40\","
//            + "allDay\":false,"
//            + "title\":\"Juan Sergio Castañeda Garza\","
//            + "status:\"Normal\","
//            + "email:\"jusergio@hotmail.com\","
//            + "phone:\"83151965\","
//            + "clientId:17,"
//            + "lastUserId: \"hlp@keynet.com.mx\"" //            + "}"
//            ;
//    private static String SENT_GET_SAVE_UPDATE_APPOINTMENTS1 = "{\"command\":\"saveEvent\",\"token\":20,\"events\":[{\"appointmentId\":\"hlp@keynet.com.mx-02-2015-00261\",\"type\":\"A\",\"start\":\"2015-02-19 15:30\",\"end\":\"2015-02-19 16:40\",\"allDay\":false,\"title\":\"Juan Sergio Castañeda Garza\",\"status\":\"Normal\",\"email\":\"jusergio@hotmail.com\",\"phone\":\"83151965\",\"clientId\":17,\"lastUserId\":\"hlp@keynet.com.mx\",\"updated\":false,\"saved\":false}]}";
//    private static String SENT_GET_SAVE_UPDATE_APPOINTMENTS2 = "{\"command\":\"saveEvent\",\"token\":20,\"events\":[{\"type\":\"A\",\"observations\":\"Some notes blah blah\",\"start\":\"2015-02-19 15:30\",\"end\":\"2015-02-19 16:40\",\"allDay\":false,\"title\":\"Juan Sergio Castañeda Garza\",\"status\":\"Normal\",\"email\":\"jusergio@hotmail.com\",\"phone\":\"83151965\",\"updated\":false,\"saved\":false}]}";



}

