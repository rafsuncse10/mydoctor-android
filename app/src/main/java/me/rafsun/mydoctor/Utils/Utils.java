package me.rafsun.mydoctor.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicInteger;

import me.rafsun.mydoctor.ui.LoginActivity;
import me.rafsun.mydoctor.ui.MainActivity;

/**
 * Created by Rafsun on 3/11/15.
 */
public class Utils {

    public static final String PREF_KEY_LOG = "LOG_PREF";
    public static final String PREF_USER = "PREF_USER";
    public static final String PREF_PASS = "PREF_PASS";

    public static boolean isSavedAtSharedPref(Activity activity){
        SharedPreferences editor = activity.getSharedPreferences(Utils.PREF_KEY_LOG, Context.MODE_PRIVATE);
        return !editor.getString(Utils.PREF_USER, "").isEmpty();
    }

    public static void gotoNextActivity(Activity activity) {
        Intent activityIntent = null;
        if (activity instanceof LoginActivity) {
            activityIntent = new Intent((LoginActivity) activity, MainActivity.class);
        } else if (activity instanceof MainActivity) {
            activityIntent = new Intent((MainActivity) activity, MainActivity.class);

        }

        if (activityIntent != null) {
            activity.startActivity(activityIntent);
            activity.finish();
        }
    }
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    @SuppressLint("NewApi")
    public static int generateViewId() {

        if (Build.VERSION.SDK_INT < 17) {
            for (; ; ) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF)
                    newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue)) {
                    return result;
                }
            }
        } else {
            return View.generateViewId();
        }

    }



    //Just to Hide any softKeyborad
    public static void hideSoftKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


//            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }


    //Just to enable touch outside on any view or viewgroup , Initialize it before return rootView
    public static void hideKeyboardOnOutSideTouch(View viewOrViewGroup , final Activity activity) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(viewOrViewGroup instanceof EditText)) {

            viewOrViewGroup.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewOrViewGroup instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) viewOrViewGroup).getChildCount(); i++) {

                View innerView = ((ViewGroup) viewOrViewGroup).getChildAt(i);

                hideKeyboardOnOutSideTouch(innerView,activity);
            }
        }
    }


    public static String sha256Hex(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

}
