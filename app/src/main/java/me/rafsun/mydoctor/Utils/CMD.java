package me.rafsun.mydoctor.Utils;

/**
 * Created by Rafsun on 3/8/15.
 */
public enum CMD {

    LOGIN,DOCTOR_INFO_GET,PATIENTS_INFO_GET,PATIENTS_APPOINTMENT_GET,PATIENTS_EVENT_SET,PATIENTS_EVENT_UPDATE
}
