package me.rafsun.mydoctor.Singletones;


import java.util.ArrayList;
import java.util.List;

import me.rafsun.mydoctor.MyWeekView.WeekViewEvent;

/**
 * Created by Rafsun on 3/3/15.
 */
public enum  WeekEventSingletone {
    INSTANCE;
    private List<WeekViewEvent> events = new ArrayList<>();

    private WeekEventSingletone() {

    }

    public List<WeekViewEvent> getInstance() {
        return events;
    }
}
