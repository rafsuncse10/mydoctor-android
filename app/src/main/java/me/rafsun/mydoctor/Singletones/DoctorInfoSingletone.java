package me.rafsun.mydoctor.Singletones;

import me.rafsun.mydoctor.json.InfoResponse;

/**
 * Created by Rafsun on 3/12/15.
 */
public enum DoctorInfoSingletone {

    INSTANCE;
    private InfoResponse infoResponse = new InfoResponse();

    private DoctorInfoSingletone() {

    }

    public InfoResponse getInstance() {
        return infoResponse;
    }
}
