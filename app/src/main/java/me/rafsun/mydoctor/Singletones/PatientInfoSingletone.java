package me.rafsun.mydoctor.Singletones;


import java.util.ArrayList;
import java.util.List;

import me.rafsun.mydoctor.json.Patient;

/**
 * Created by Rafsun on 3/3/15.
 */
public enum PatientInfoSingletone {

    INSTANCE;
    private List<Patient> patients = new ArrayList<>();

    private PatientInfoSingletone() {

    }

    public List<Patient> getInstance() {
        return patients;
    }

}
