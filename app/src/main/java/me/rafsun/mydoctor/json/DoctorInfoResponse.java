/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class DoctorInfoResponse {
    String userCompleteName;
    String userId;
    Doctor doctor;
    Address address;
    Settings settings;

    public String getUserCompleteName() {
        return userCompleteName;
    }

    public void setUserCompleteName(String userCompleteName) {
        this.userCompleteName = userCompleteName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    @Override
    public String toString() {
        return "DoctorInfoResponse{" + "userCompleteName=" + userCompleteName + ", userId=" + userId + ", doctor=" + doctor + ", address=" + address + ", settings=" + settings + '}';
    }
    
    
    
}
