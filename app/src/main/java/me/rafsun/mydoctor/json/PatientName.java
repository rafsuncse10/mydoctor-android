package me.rafsun.mydoctor.json;

/**
 * Created by Rafsun on 3/11/15.
 */
public class PatientName {
    int eventPosition;
    String Patient;

    public PatientName(int eventPosition, String patient) {
        this.eventPosition = eventPosition;
        Patient = patient;
    }

    public int getEventPosition() {
        return eventPosition;
    }

    public void setEventPosition(int eventPosition) {
        this.eventPosition = eventPosition;
    }

    public String getPatient() {
        return Patient;
    }

    public void setPatient(String patient) {
        Patient = patient;
    }

    @Override
    public String toString() {
        return "PatientName{" +
                "eventPosition=" + eventPosition +
                ", Patient='" + Patient + '\'' +
                '}';
    }
}
