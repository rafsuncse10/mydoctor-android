/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class FiscalInfo {
    String rfc;
    String street;
    String numberStreetOut;
    String numberStreetIn;
    String suburb;
    String county;
    String zipCode;

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumberStreetOut() {
        return numberStreetOut;
    }

    public void setNumberStreetOut(String numberStreetOut) {
        this.numberStreetOut = numberStreetOut;
    }

    public String getNumberStreetIn() {
        return numberStreetIn;
    }

    public void setNumberStreetIn(String numberStreetIn) {
        this.numberStreetIn = numberStreetIn;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {

//        return "FiscalInfo{" + "rfc=" + rfc + ", street=" + street + ", numberStreetOut=" + numberStreetOut + ", numberStreetIn=" + numberStreetIn + ", suburb=" + suburb + ", county=" + county + ", zipCode=" + zipCode + '}';
//        return "Address Info : " + "rfc=" + rfc + ", street=" + street + ", numberStreetOut=" + numberStreetOut + ", numberStreetIn=" + numberStreetIn + ", suburb=" + suburb + ", county=" + county + ", zipCode=" + zipCode ;
        return "Address Info : " + "rfc=" + rfc + ", street=" + street + ", numberStreetOut=" + numberStreetOut + ", numberStreetIn=" + numberStreetIn + ", suburb=" + suburb + ", county=" + county + ", zipCode=" + zipCode ;
    }
    
    
    
}
