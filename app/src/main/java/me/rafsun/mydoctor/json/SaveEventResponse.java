package me.rafsun.mydoctor.json;

import java.util.List;

/**
 * Created by Rafsun on 3/8/15.
 */
public class SaveEventResponse {
    List<Appointment> events;

    @Override
    public String toString() {
        return "SaveEventResponse{" + "events=" + events + '}';
    }

    public List<Appointment> getEvents() {
        return events;
    }

    public void setEvents(List<Appointment> events) {
        this.events = events;
    }
}
