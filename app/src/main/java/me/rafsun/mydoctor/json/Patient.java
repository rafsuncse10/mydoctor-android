/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class Patient {
    int clientId;
    String name;
    String lastName;
    String surName;
    String birthdate;
    String gender;
    String job;
    String phone;
    String cellphone;
    String email;
    String referenceName;
    String maritalStatus;
    String lastUserId;
    FiscalInfo fiscalInfo;
    ChildInfo childInfo;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getLastUserId() {
        return lastUserId;
    }

    public void setLastUserId(String lastUserId) {
        this.lastUserId = lastUserId;
    }

    public FiscalInfo getFiscalInfo() {
        return fiscalInfo;
    }

    public void setFiscalInfo(FiscalInfo fiscalInfo) {
        this.fiscalInfo = fiscalInfo;
    }

    public ChildInfo getChildInfo() {
        return childInfo;
    }

    public void setChildInfo(ChildInfo childInfo) {
        this.childInfo = childInfo;
    }

    @Override
    public String toString() {
        return "Patient{" + "clientId=" + clientId + ", name=" + name + ", lastName=" + lastName + ", surName=" + surName + ", birthdate=" + birthdate + ", gender=" + gender + ", job=" + job + ", phone=" + phone + ", cellphone=" + cellphone + ", email=" + email + ", referenceName=" + referenceName + ", maritalStatus=" + maritalStatus + ", lastUserId=" + lastUserId + ", fiscalInfo=" + fiscalInfo + ", childInfo=" + childInfo + '}';
    }
    
    
}
