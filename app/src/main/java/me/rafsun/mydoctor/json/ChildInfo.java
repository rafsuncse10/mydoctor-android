/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class ChildInfo {
    String faterName;
    String fatherEmail;
    String fatherCellPhone;
    String fatherHomePhone;
    
    String motherName;
    String motherEmail;
    String motherCellPhone;
    String motherHomePhone;

    public String getFaterName() {
        return faterName;
    }

    public void setFaterName(String faterName) {
        this.faterName = faterName;
    }

    public String getFatherEmail() {
        return fatherEmail;
    }

    public void setFatherEmail(String fatherEmail) {
        this.fatherEmail = fatherEmail;
    }

    public String getFatherCellPhone() {
        return fatherCellPhone;
    }

    public void setFatherCellPhone(String fatherCellPhone) {
        this.fatherCellPhone = fatherCellPhone;
    }

    public String getFatherHomePhone() {
        return fatherHomePhone;
    }

    public void setFatherHomePhone(String fatherHomePhone) {
        this.fatherHomePhone = fatherHomePhone;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getMotherEmail() {
        return motherEmail;
    }

    public void setMotherEmail(String motherEmail) {
        this.motherEmail = motherEmail;
    }

    public String getMotherCellPhone() {
        return motherCellPhone;
    }

    public void setMotherCellPhone(String motherCellPhone) {
        this.motherCellPhone = motherCellPhone;
    }

    public String getMotherHomePhone() {
        return motherHomePhone;
    }

    public void setMotherHomePhone(String motherHomePhone) {
        this.motherHomePhone = motherHomePhone;
    }

    @Override
    public String toString() {
        return "ChildInfo{" + "fatherEmail=" + fatherEmail + ", fatherCellPhone=" + fatherCellPhone + ", fatherHomePhone=" + fatherHomePhone + ", motherName=" + motherName + ", motherEmail=" + motherEmail + ", motherCellPhone=" + motherCellPhone + ", motherHomePhone=" + motherHomePhone + '}';
    }
    
    
}
