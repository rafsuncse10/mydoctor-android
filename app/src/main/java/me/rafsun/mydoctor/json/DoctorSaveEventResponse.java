package me.rafsun.mydoctor.json;

/**
 * Created by Rafsun on 3/8/15.
 */
public class DoctorSaveEventResponse {
    int token;
    SaveEventResponse saveEventResponse;

    @Override
    public String toString() {
        return "DoctorSaveEventResponse{" + "token=" + token + ", saveEventResponse=" + saveEventResponse + '}';
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public SaveEventResponse getSaveEventResponse() {
        return saveEventResponse;
    }

    public void setSaveEventResponse(SaveEventResponse saveEventResponse) {
        this.saveEventResponse = saveEventResponse;
    }

}
