/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

import java.util.List;

/**
 *
 * @author Rafsun
 */
public class DoctorAppointmentsResponse {
    String doctorId;
    int startMonth;
    int endMonth;
    int startYear;
    int endYear;
    List<Appointment> appointments;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    @Override
    public String toString() {
        return "DoctorAppointmentsResponse{" + "doctorId=" + doctorId + ", startMonth=" + startMonth + ", endMonth=" + endMonth + ", startYear=" + startYear + ", endYear=" + endYear + ", appointments=" + appointments + '}';
    }
    
    
    
}
