/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class DoctorSearchAllPatientsResponse {
    int token;
    SearchAllPatientsResponse searchAllPatientsResponse;

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public SearchAllPatientsResponse getSearchAllPatientsResponse() {
        return searchAllPatientsResponse;
    }

    public void setSearchAllPatientsResponse(SearchAllPatientsResponse searchAllPatientsResponse) {
        this.searchAllPatientsResponse = searchAllPatientsResponse;
    }

    @Override
    public String toString() {
        return "DoctorSearchAllPatientsResponse{" + "token=" + token + ", searchAllPatientsResponse=" + searchAllPatientsResponse + '}';
    }
    
    
}
