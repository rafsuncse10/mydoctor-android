/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class LoginResponse {
    int token;
    LoginResponseDescription loginResponse;

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public LoginResponseDescription getLoginResponse() {
        return loginResponse;
    }

    public void setLoginResponse(LoginResponseDescription loginResponse) {
        this.loginResponse = loginResponse;
    }

    @Override
    public String toString() {
        return "LoginResponse{" + "token=" + token + ", loginResponse=" + loginResponse + '}';
    }
    
    
    
}
