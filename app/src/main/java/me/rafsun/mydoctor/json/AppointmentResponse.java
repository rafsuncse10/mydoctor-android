/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class AppointmentResponse {
    int token ;
    DoctorAppointmentsResponse doctorAppointmentsResponse;

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public DoctorAppointmentsResponse getDoctorAppointmentsResponse() {
        return doctorAppointmentsResponse;
    }

    public void setDoctorAppointmentsResponse(DoctorAppointmentsResponse doctorAppointmentsResponse) {
        this.doctorAppointmentsResponse = doctorAppointmentsResponse;
    }

    @Override
    public String toString() {
        return "AppointmentResponse{" + "token=" + token + ", doctorAppointmentsResponse=" + doctorAppointmentsResponse + '}';
    }
    
    
}
