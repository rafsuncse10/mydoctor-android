/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

import java.util.List;

/**
 *
 * @author Rafsun
 */
public class SearchAllPatientsResponse {
    List<Patient> patients;

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    @Override
    public String toString() {
        return "SearchAllPatientsResponse{" + "patients=" + patients + '}';
    }
    
    
}
