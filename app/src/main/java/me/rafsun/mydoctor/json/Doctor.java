/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 * @author Rafsun
 */
public class Doctor {
    String id;
    String name;
    String middleName;
    String lastName;
    String surName;
    String gender;
    String license;
    String rfc;
    String speciality;
    String emailOffice;
    String emailPersonal;
    String phoneOffice;
    String phoneMobile;
    String phoneHome;
    String renewwalDate;
    String logo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getEmailOffice() {
        return emailOffice;
    }

    public void setEmailOffice(String emailOffice) {
        this.emailOffice = emailOffice;
    }

    public String getEmailPersonal() {
        return emailPersonal;
    }

    public void setEmailPersonal(String emailPersonal) {
        this.emailPersonal = emailPersonal;
    }

    public String getPhoneOffice() {
        return phoneOffice;
    }

    public void setPhoneOffice(String phoneOffice) {
        this.phoneOffice = phoneOffice;
    }

    public String getPhoneMobile() {
        return phoneMobile;
    }

    public void setPhoneMobile(String phoneMobile) {
        this.phoneMobile = phoneMobile;
    }

    public String getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    public String getRenewwalDate() {
        return renewwalDate;
    }

    public void setRenewwalDate(String renewwalDate) {
        this.renewwalDate = renewwalDate;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public String toString() {
        return "Doctor{" + "id=" + id + ", name=" + name + ", middleName=" + middleName + ", lastName=" + lastName + ", surName=" + surName + ", gender=" + gender + ", license=" + license + ", rfc=" + rfc + ", speciality=" + speciality + ", emailOffice=" + emailOffice + ", emailPersonal=" + emailPersonal + ", phoneOffice=" + phoneOffice + ", phoneMobile=" + phoneMobile + ", phoneHome=" + phoneHome + ", renewwalDate=" + renewwalDate + ", logo=" + logo + '}';
    }


}
