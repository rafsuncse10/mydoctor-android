/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.rafsun.mydoctor.json;

/**
 *
 * @author Rafsun
 */
public class InfoResponse {
    int token;
    DoctorInfoResponse doctorInfoResponse;

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public DoctorInfoResponse getDoctorInfoResponse() {
        return doctorInfoResponse;
    }

    public void setDoctorInfoResponse(DoctorInfoResponse doctorInfoResponse) {
        this.doctorInfoResponse = doctorInfoResponse;
    }

    @Override
    public String toString() {
        return "InfoResponse{" + "token=" + token + ", doctorInfoResponse=" + doctorInfoResponse + '}';
    }
    
    
    
}
